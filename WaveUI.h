/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_GRAPH                      2       /* control type: graph, callback function: (none) */
#define  PANEL_COMMANDBUTTON              3       /* control type: command, callback function: endProgram */
#define  PANEL_SAMPLE_RATE                4       /* control type: numeric, callback function: populateSampleRate */
#define  PANEL_FREQUENCY                  5       /* control type: numeric, callback function: populateFrequency */
#define  PANEL_EPOCH_LENGTH               6       /* control type: numeric, callback function: populateEpochLength */
#define  PANEL_DUTY_CYCLE                 7       /* control type: numeric, callback function: populateDutyCycle */
#define  PANEL_BASE_LINE                  8       /* control type: numeric, callback function: populateBaseLine */
#define  PANEL_AMPLITUDE                  9       /* control type: numeric, callback function: populateAmplitude */
#define  PANEL_COMMANDBUTTON_2            10      /* control type: command, callback function: processValues */
#define  PANEL_COMMANDBUTTON_PLOT         11      /* control type: command, callback function: Callback_Plot_Array */
#define  PANEL_BUTTON_GEN_SQUARE          12      /* control type: command, callback function: Callback_Generate_Square */
#define  PANEL_STRIP_TEST                 13      /* control type: numeric, callback function: stripChartValue */
#define  PANEL_STOPBUTTON_4               14      /* control type: command, callback function: TIMER_RUPT */
#define  PANEL_LED                        15      /* control type: LED, callback function: (none) */
#define  PANEL_SAMPLING_NUMERIC           16      /* control type: numeric, callback function: timer_control */
#define  PANEL_NUMERIC_AO_CHAN            17      /* control type: numeric, callback function: Callback_EnterAO */
#define  PANEL_NUMERIC_ENTER_AO_VAL       18      /* control type: numeric, callback function: Callback_Enter_AO_Val */
#define  PANEL_COMMANDBUTTON_STOP         19      /* control type: command, callback function: Callback_STOP_Activate */
#define  PANEL_LED_DEBUG                  20      /* control type: LED, callback function: (none) */
#define  PANEL_BUTTON_UPDATE_AO           21      /* control type: command, callback function: Callback_Update_AO */
#define  PANEL_BUTTON_RAMP                22      /* control type: command, callback function: Callback_GenerateRamp */
#define  PANEL_NUMERIC_AI                 23      /* control type: numeric, callback function: (none) */
#define  PANEL_NUMERIC_AI_CHAN_SEL        24      /* control type: numeric, callback function: Callback_Select_AI_Channel */
#define  PANEL_BUTTON_MEASURE_AI          25      /* control type: command, callback function: Callback_Measure_AI */
#define  PANEL_BUTTON_GEN_RAMP            26      /* control type: command, callback function: Callback_Generate_Ramp */
#define  PANEL_RADIOBUTTON                27      /* control type: radioButton, callback function: button_one */
#define  PANEL_RADIOBUTTON_2              28      /* control type: radioButton, callback function: button_two */
#define  PANEL_RADIOBUTTON_3              29      /* control type: radioButton, callback function: button_three */
#define  PANEL_DECORATION                 30      /* control type: deco, callback function: (none) */
#define  PANEL_BINARYSWITCH               31      /* control type: binary, callback function: milky_lcd_switch */
#define  PANEL_LED_2                      32      /* control type: LED, callback function: (none) */
#define  PANEL_BINARYSWITCH_2             33      /* control type: binary, callback function: binarySwitchControl2 */
#define  PANEL_DECORATION_4               34      /* control type: deco, callback function: (none) */
#define  PANEL_TEXTMSG_2                  35      /* control type: textMsg, callback function: (none) */
#define  PANEL_STRIPCHART                 36      /* control type: strip, callback function: (none) */
#define  PANEL_RINGSLIDE_SEL_OUTPUT       37      /* control type: slide, callback function: Callback_SelectOutput */
#define  PANEL_ENABLE_POTENTIOMETERS      38      /* control type: textButton, callback function: Callback_EnableDisablePotentiometers */
#define  PANEL_COMMANDBUTTON_DEBUG        39      /* control type: command, callback function: Callback_DebugProgram */
#define  PANEL_TIMER                      40      /* control type: timer, callback function: timerFunction */
#define  PANEL_TEXTMSG                    41      /* control type: textMsg, callback function: (none) */
#define  PANEL_STRING_SCALE               42      /* control type: string, callback function: (none) */
#define  PANEL_NUMERIC_Output_Base        43      /* control type: numeric, callback function: (none) */
#define  PANEL_NUMERIC_Output_Ampl        44      /* control type: numeric, callback function: (none) */
#define  PANEL_BUTTON_OTHER_OPEN          45      /* control type: command, callback function: Callback_OpenOtherEye */
#define  PANEL_BUTTON_CLOSE_OTHER         46      /* control type: command, callback function: Callback_CloseOtherEye */
#define  PANEL_BUTTON_OPEN_BOTH           47      /* control type: command, callback function: Callback_OpenBothEyes */


     /* Control Arrays: */

#define  CTRLARRAY                        1

     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK binarySwitchControl2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK button_one(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK button_three(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK button_two(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_CloseOtherEye(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_DebugProgram(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_EnableDisablePotentiometers(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Enter_AO_Val(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_EnterAO(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Generate_Ramp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Generate_Square(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_GenerateRamp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Measure_AI(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_OpenBothEyes(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_OpenOtherEye(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Plot_Array(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Select_AI_Channel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_SelectOutput(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_STOP_Activate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Callback_Update_AO(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK endProgram(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK milky_lcd_switch(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateAmplitude(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateBaseLine(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateDutyCycle(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateEpochLength(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateFrequency(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK populateSampleRate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK processValues(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK stripChartValue(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK timer_control(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK TIMER_RUPT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK timerFunction(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
