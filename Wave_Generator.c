//------Libraries------
#include <formatio.h>
#include <NIDAQmx.h>
#include <utility.h>
#include <ansi_c.h>
#include <userint.h>
#include <analysis.h>
#include <math.h>
#include <cvirte.h>
#include "Wave_Generator.h"
#include "WaveUI.h"
//#include <stdio.h>
//#include <stdlib.h>

//------Constants------
#define TABLE_SIZE 2500
#define TWO_PI (3.14159 * 2)
#define MaxScans  3 //DAQ 
#define MinFreq  0.2
#define MaxFreq  30  
#define MaxDuty  98	   // duty cycle in %
#define MinDuty  1
#define MinAmplitude  0.1
#define MaxAmplitude  10
#define MaxInputAmplitudeInVolts 10  // as read by the ADC


//------Variables------
static int MAIN_PANEL; //panel handle used to represent main window
static double SQUARE_ARRAY[TABLE_SIZE]; //array to hold square wave
static double RAMP_ARRAY[TABLE_SIZE]; //array to hold ramp
static int RAMP_COUNTER=0;
static int binary_switch_2=1;
static int MILKY_LCD=1; //lcd is 1, milky is 0
static int EYE_SELECTOR; //0 is right, 1 is left,  2 is both, 
static int *EYE_STATE= &EYE_SELECTOR;
static int MILKY_SPACING=30; // for SR = 1000; 15 for SR = 500
static int PotentiometersEnabled = 0;

//Square wave parameters
static double SAMPLE_RATE = 1000; //i.e 1 ms is SAMPLE_RATE= 1000; SHOULD REMAIN FIXED FOR NOW; connected to timer interval!!
static double SAMPLE_RATE_ADC = 10000;
static double FREQUENCY = 2; //Frequency of the waveform, per second (Hz)
static double FREQUENCY_PREVI = 2;
					  
					   
static double EPOCH_LENGTH; //i.e 1 second, duration at peak amplitude.
static double DUTY_CYCLE = 0.5;  
static double DUTY_CYCLE_PREVI = 0.5;
static double BASE_LINE = 0;
static double AMPLITUDE = 5; 
static double AMPLITUDE_PREVI = 5;

static double RealAnalogOutputBaseline, RealAnalogOutputAmplitude, TempAmplitude, TempBaseline, PreviousBaseline, PreviousAmplitude ;
																		   
static unsigned long numScans = MaxScans; //DAQ
	
static double STRIP_VAL = 0; //UNUSED
static double timer_numeric_value; //used to control the timer field.

int sample_index=0;// this is to keep track of indexed points   in arrays
int sample_index_2=0;// this is to keep track of indexed points  in arrays


double T; //Time= 1/Frequency for use in GenerateSquareWave
int MiddleSample, EndSample; //Middlesample and Endsample for use in GenerateSquareWave						 

//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
// Related to NI DAQmx:

int i, j, ii, jj, l;
int STOP_FLAG= 1;
int StopFlag = 0;
int change_flag= 0;

float64 AnalogOutputValue = 0.0;
float64 AnalogInputValue;
short int AnalogOutputChNmb;
static unsigned char Numb_AI_Chan = 0;  // was short
static unsigned char Numb_AO_Chan = 0;  // was short
static TaskHandle daqTask;
static TaskHandle taskHandleADC2; 
// static TaskHandle taskHandleAO=0;
static TaskHandle taskHandleAO_0 = 0;   
static TaskHandle taskHandleAO_1 = 0;   
static float64	  WaveformsLong [16][MaxScans]; //DAQ   

double      AOmin = 0.0;	     // now global  
double 		AOmax = 5.0;		 // now global
double 		AImin = -10.0;   // now global 
double 		AImax = 10.0;	 // now global  

char        *chan0  = "Dev1/ao0";	  // now made global 
char        *chan1  = "Dev1/ao1";	  // now made global 
char        AIchan[256];

uInt32      sampsPerChan;
uInt32      numChannels;
 
static int ColorArray[8] = {VAL_GREEN, VAL_YELLOW, VAL_CYAN, VAL_WHITE, VAL_BLUE, VAL_MAGENTA, VAL_GRAY, VAL_RED};          

char        errBuff[2048]={'\0'};	  // global   
char 		string_buffer [7];     // for converting short numbers to strings

//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#  


//------Function declarations------
void GenerateSquareWave (double SR, double Freq, double Duty, double Base, double Amp); 
void GenerateRamp (double SR, double Freq, double Base, double Amp);
void PlotMyArray (void);
void clearArray(void);
void SoundAlarm(int numb_sounds); 
void GenerateRampToWaveformsLong (int ChNumbero);
void AnalogOutput(int AnChannel, float64 data)  ;
void ReadAnalogInputs();

void OpenOtherEye();
void CloseOtherEye();
void OpenBothEyes();

// =======================================  MAIN  ====================================

int main(int argc, char *argv[]){
	
	if ((MAIN_PANEL= LoadPanel(0, "WaveUI.uir", PANEL)) <= 0 )
		return -1;
	
	DisplayPanel(MAIN_PANEL); //loadpanel loads into memory, display shows it
  
// Initialization	
	
	GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE); 
	GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE);
	RealAnalogOutputAmplitude = (AMPLITUDE + BASE_LINE - 2.5) * 4;
	SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Ampl, RealAnalogOutputAmplitude);  
	RealAnalogOutputBaseline = (BASE_LINE - 2.5) * 4;  
	SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Base, RealAnalogOutputBaseline);  
	
	GetCtrlVal(MAIN_PANEL, PANEL_ENABLE_POTENTIOMETERS, &PotentiometersEnabled);
	SetCtrlVal(MAIN_PANEL, PANEL_LED, 1);
	
	GetCtrlVal(MAIN_PANEL, PANEL_SAMPLING_NUMERIC , &timer_numeric_value);
	MILKY_SPACING = RoundRealToNearestInteger (15.0/(timer_numeric_value));
    snprintf(string_buffer, 5, "%f", timer_numeric_value);
	SetCtrlVal(MAIN_PANEL, PANEL_STRING_SCALE,string_buffer);      // update time scale indicator	
	SetCtrlAttribute(MAIN_PANEL, PANEL_TIMER, ATTR_INTERVAL, timer_numeric_value/1000);  // set timer
	SAMPLE_RATE =  RoundRealToNearestInteger (1/(timer_numeric_value/1000));
	SetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, SAMPLE_RATE);
	
	GetCtrlVal(MAIN_PANEL, PANEL_RINGSLIDE_SEL_OUTPUT, &EYE_SELECTOR);
	GetCtrlVal(MAIN_PANEL, PANEL_BINARYSWITCH, &MILKY_LCD);

	GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);  // using the default parameters
	GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);
	PlotMyArray();
	
	//==============    INITIALIZE NIDAQ TASKS  ======================
	
	//---------- Initialize Analog Output Tasks ----------------------------------
	// Chammel 0
	DAQmxCreateTask("",&taskHandleAO_0);
	DAQmxCreateAOVoltageChan(taskHandleAO_0,chan0,"",AOmin,AOmax,DAQmx_Val_Volts,"");   // output to AO chan 0 
	DAQmxStartTask(taskHandleAO_0 );
	// Channel 1
	DAQmxCreateTask("",&taskHandleAO_1);
	DAQmxCreateAOVoltageChan(taskHandleAO_1,chan1,"",AOmin,AOmax,DAQmx_Val_Volts,"");   // output to AO chan 1 
	DAQmxStartTask(taskHandleAO_1 );
	
	
		//DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle,chan,"",min,max,DAQmx_Val_Volts,""));
		//DAQmxCreateAOVoltageChan(taskHandle,chan,"",min,max,DAQmx_Val_Volts,"");
		
		//if (AnChannel==0)  
		//    DAQmxCreateAOVoltageChan(taskHandleAO,chan0,"",min,max,DAQmx_Val_Volts,"");   // output to AO chan 0   // should be separated?
		//  else
		//	DAQmxCreateAOVoltageChan(taskHandleAO,chan1,"",min,max,DAQmx_Val_Volts,"");   // output to AO chan 1  
		/*********************************************/
		// DAQmx Start Code
		/*********************************************/
		//DAQmxErrChk (DAQmxStartTask(taskHandle));
		// DAQmxStartTask(taskHandleAO); 
	
 	
//---------- Initialize Analog Input Task ---------------------------------- 
	DAQmxCreateTask("",&taskHandleADC2);												  // NEEDS ATTENTION  
	// Default configuration (behaves as differential)
	//DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandleADC2,chan,"",DAQmx_Val_Cfg_Default,min,max,DAQmx_Val_Volts,NULL));
	         // forced differential configuration:
	         // DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandleADC2,chan,"",DAQmx_Val_Diff,min,max,DAQmx_Val_Volts,NULL));     	  
	Fmt(AIchan,"%s<Dev1/ai0,Dev1/ai1,Dev1/ai2,Dev1/ai3");	// ONLY FOUR INPUT CHANNELS ALLOWED (for speed)																			  // NEEDS ATTENTION   
	DAQmxCreateAIVoltageChan (taskHandleADC2, AIchan, "", DAQmx_Val_RSE, AImin, AImax, DAQmx_Val_Volts, NULL);			// Single-ended input
	// forced differential configuration:
	//DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandleADC2,chan,"",DAQmx_Val_RSE,min,max,DAQmx_Val_Volts,NULL));

	 sampsPerChan = numScans;  // numScansFFT + 1 scanning cycle           
	//DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandleADC2,"",rate,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,sampsPerChan));    	  // NEEDS ATTENTION  
	DAQmxCfgSampClkTiming(taskHandleADC2,"",SAMPLE_RATE_ADC,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,sampsPerChan) ;
	
	/*
    // set timing
	if (InternalPace==1)  // ADC paced internally
	   DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandleADC2,"",rate,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,sampsPerChan));
	 else		  // ADC paced externally 
	   DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandleADC2,clockSource,rate,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,sampsPerChan)); 	 
	*/	 
		
	//DAQmxErrChk (DAQmxGetTaskAttribute(taskHandleADC2,DAQmx_Task_NumChans,&numChannels));  // Obtains the value of a task attribute (number of channels = numChannels, local)
	DAQmxGetTaskAttribute(taskHandleADC2,DAQmx_Task_NumChans,&numChannels);  // Obtains the value of a task attribute (number of channels = numChannels, local)
	
	//DAQmxStartTask(taskHandleADC2);
	
		
    //================================================================
	
	OpenBothEyes(); 
	
	RunUserInterface();
}

/* Debugging strip val
void squareWaveStrip(double samp, double freq, double duty, double base, double amp){
	
	PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, STRIP_VAL);
}
*/


int CVICALLBACK endProgram (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{													
	switch (event)
	{
		case EVENT_COMMIT:
			
			// Close Analog Output Task
			    DAQmxStopTask(taskHandleAO_0);	 // AO_0       
	            DAQmxClearTask(taskHandleAO_0);  
				DAQmxStopTask(taskHandleAO_1);	 // AO_1      
	            DAQmxClearTask(taskHandleAO_1); 
			// Closing Analog Input Task
			   // DAQmxStopTask(taskHandleADC2);  // done after every reade
			   DAQmxClearTask(taskHandleADC2);
			
			DiscardPanel (MAIN_PANEL); //removes from screen and memory
			CloseCVIRTE (); //releases memory allocated by CVI Runtime Engine
			if (QuitUserInterface(0)!= 0){// return on quit button press
				return -1;
			}
			break;
	}
	return 0;
}

//The sole purpose of processValues is to update, post factum, the variables
//that correspond to their respective numeric fields in the UIR.
int CVICALLBACK processValues (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			//GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE);
			//GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			//GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			//GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			//GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE); 
			//GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE); 
			
			printf("\n\nIMPORTANT PROGRAM PARAMETERS:\n\n");
			printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			printf("FREQUENCY = %f\n", FREQUENCY);
			printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);
			printf("BASE_LINE = %f\n", BASE_LINE);
			printf("AMPLITUDE = %f\n", AMPLITUDE);
			printf("MILKY_LCD = %i\n", MILKY_LCD); 
			printf("MILKY_SPACING = %i\n", MILKY_SPACING);   
			printf("\n\n");
			
			/*
			clearArray();
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);
			*/
			
			break;
	}																  																															   
	return 0;
}


void clearArray(){
	for(int i=0; i<TABLE_SIZE; i++){
		SQUARE_ARRAY[i]=0;
		RAMP_ARRAY[i]=0;
	}
}

//singularly updates the SAMPLE RATE real time.
int CVICALLBACK populateSampleRate (int panel, int control, int event,
									void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE);
			clearArray();
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);
			DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_NO_DRAW);
			PlotMyArray();
			//printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			break;
	}
	return 0;
}

//singularly updates the FREQUENCY
int CVICALLBACK populateFrequency (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			FREQUENCY_PREVI = FREQUENCY;
			clearArray();
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);    
			DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_IMMEDIATE_DRAW);
			PlotMyArray();
			//printf("FREQUENCY = %f\n", FREQUENCY);     
			break;
	}
	return 0;
}

//singularly updates the EPOCH LENGTH. 
int CVICALLBACK populateEpochLength (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			clearArray();
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);    
			DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_NO_DRAW);
			PlotMyArray();
			//printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			break;
	}
	return 0;
}

//singularly updates the DUTY CYCLE
int CVICALLBACK populateDutyCycle (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			DUTY_CYCLE = DUTY_CYCLE/100.0;
			if(MILKY_LCD==1)   // LCD shutters are open at 0 V,  active low !!)
			   { DUTY_CYCLE = 1.00 - DUTY_CYCLE;   }
			clearArray();
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);    
			DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_NO_DRAW);
			PlotMyArray();
			//printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);     
			break;
	}
	return 0;
}

//singularly updates the BASE LINE
int CVICALLBACK populateBaseLine (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
		 if (MILKY_LCD == 1) //  only when LCD glasses;  do not change baseline when working with Milky glasses!!
		 {
			PreviousBaseline =  BASE_LINE;
			GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE);
			TempBaseline = (BASE_LINE - 2.5) * 4;
            TempAmplitude = (AMPLITUDE + BASE_LINE - 2.5) * 4;  
			if ((TempAmplitude < 10.001) & (TempAmplitude > -10.001))  // not using TempBaseline for protection yet !!
			{
			   RealAnalogOutputBaseline = TempBaseline;  
			   SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Base, RealAnalogOutputBaseline);  
			   RealAnalogOutputAmplitude = TempAmplitude;
			   SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Ampl, RealAnalogOutputAmplitude);
			   clearArray();
			   GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			   GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);    
			   DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_NO_DRAW);
			   PlotMyArray();
			}
			else	// DID NOT WORK properly
			 { Delay(0.2);
			   SetCtrlVal (MAIN_PANEL, PANEL_BASE_LINE, PreviousBaseline);    // write back previous baseline value
			 }
			//printf("BASE_LINE = %f\n", BASE_LINE);
		 } // of case when working with LCD (no action in the case of mily glasses
		break;
	}
	return 0;
}



//singularly updates the AMPLITUDE real time.
int CVICALLBACK populateAmplitude (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			PreviousAmplitude =  AMPLITUDE;  
			GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE); 
			TempAmplitude = (AMPLITUDE + BASE_LINE - 2.5) * 4;
			if ((TempAmplitude < 10.001) & (TempAmplitude > -10.001))  // not using TempBaseline for protection yet !! 
			{ RealAnalogOutputAmplitude = TempAmplitude ;
			  SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Ampl, RealAnalogOutputAmplitude);          
			  clearArray();
			  GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			  GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);    
			  DeleteGraphPlot (MAIN_PANEL, PANEL_GRAPH, -1, VAL_NO_DRAW);
			  PlotMyArray();
			}
			else		   // DID NOT WORK  properly
			  {  Delay(0.2); // Beep();
			     SetCtrlVal (MAIN_PANEL, PANEL_AMPLITUDE, PreviousAmplitude);  // write back previous amplitude value
			   }
			//printf("AMPLITUDE = %f\n", AMPLITUDE);   
			break;
	}
	return 0;
}


//The timer tick actions are controlled here.
int CVICALLBACK timerFunction (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2){
	if (event == EVENT_TIMER_TICK){
		if(STOP_FLAG == 0){
		//	GetCtrlVal(MAIN_PANEL,PANEL_STRIP_TEST, &STRIP_VAL);
		//  PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, STRIP_VAL);
		
		if(binary_switch_2==1){   // SQUARE array
			PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, SQUARE_ARRAY[sample_index]);
			
			switch (EYE_SELECTOR) {   // 0 = RE,  1 = LE;  2 = both eyes (channels
			case 0: AnalogOutput(0, SQUARE_ARRAY[sample_index]);  // send value to analog output 0 (RE)
					break;
		   	case 1: AnalogOutput(1, SQUARE_ARRAY[sample_index]);  // send value to analog output 1 (LE)
					break;
			case 2: AnalogOutput(0, SQUARE_ARRAY[sample_index]);  // send value to both analog outputs  (RE and LE)
					AnalogOutput(1, SQUARE_ARRAY[sample_index]);  
					break;
			//default:
			}	
			
			//ReadAnalogInputs();
			if(sample_index<EndSample){
					sample_index++;
				}
			  else{							  // end of cycle
				if (PotentiometersEnabled)	  // only if potentiometer input is enabled
				{	
					ReadAnalogInputs();   // but do the changes only if there is a significant difference in any of theparameters input via the pots
					 if  ( (  ((FREQUENCY - FREQUENCY_PREVI) > 0.3)     || ((FREQUENCY_PREVI - FREQUENCY) > 0.3 )  )	  	    ||        // if any ||diff|| > threshold
						   (  ((DUTY_CYCLE - DUTY_CYCLE_PREVI) > 0.005) || ((DUTY_CYCLE_PREVI - DUTY_CYCLE) > 0.005 )  )  || 
						   (  ((AMPLITUDE - AMPLITUDE_PREVI) > 0.1)     || ((AMPLITUDE_PREVI - AMPLITUDE) > 0.1 )  )
						 )
					 { 
					   SetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, FREQUENCY);     // moved from ReadAnalogInputs() to timer interrupt routine (here) 
					   //SetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, DUTY_CYCLE);  // already displayed in ReadAnalogInputs()  (times 100)
					   SetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, AMPLITUDE);     // moved from ReadAnalogInputs() to timer interrupt routine (here)  
					   clearArray();
					   GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);	  // GENERATE SQUARE WAVE
					   PlotMyArray ();
					   FREQUENCY_PREVI  = FREQUENCY;
					   DUTY_CYCLE_PREVI = DUTY_CYCLE; 
					   AMPLITUDE_PREVI  = AMPLITUDE;
					 }
				  } // end of block when potentiometers are enabled
				sample_index=0;
				}
		}
		else if(binary_switch_2==0){   // RAMP
			PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, RAMP_ARRAY[sample_index_2]);
			
			switch (EYE_SELECTOR) {   // 0 = RE,  1 = LE;  2 = both eyes (channels
			case 0: AnalogOutput(0, RAMP_ARRAY[sample_index_2]);  // send value to analog output 0 (RE)
					break;
		   	case 1: AnalogOutput(1, RAMP_ARRAY[sample_index_2]);  // send value to analog output 1 (LE)
					break;
			case 2: AnalogOutput(0, RAMP_ARRAY[sample_index_2]);  // send value to both analog outputs  (RE and LE)
					AnalogOutput(1, RAMP_ARRAY[sample_index_2]);  
					break;
			//default:
			}	
			
			if(sample_index_2<RAMP_COUNTER){ 
				sample_index_2++;
			}
				else{   					  // end of cycle
				if (PotentiometersEnabled)	  // only if potentiometer input is enabled
				{	
					ReadAnalogInputs();   // but do the changes only if there is a significant difference in any of theparameters input via the pots
					 if  ( (  ((FREQUENCY - FREQUENCY_PREVI) > 0.3)     || ((FREQUENCY_PREVI - FREQUENCY) > 0.3 )  )	  	    ||        // if any ||diff|| > threshold
						// (  ((DUTY_CYCLE - DUTY_CYCLE_PREVI) > 0.005) || ((DUTY_CYCLE_PREVI - DUTY_CYCLE) > 0.005 )  )  || 
						   (  ((AMPLITUDE - AMPLITUDE_PREVI) > 0.1)     || ((AMPLITUDE_PREVI - AMPLITUDE) > 0.1 )  )
						 )
					 { 
					   SetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, FREQUENCY);      // moved from ReadAnalogInputs() to timer interrupt routine (here) 
					   //SetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, DUTY_CYCLE);  // already displayed in ReadAnalogInputs()  (times 100), BUT NOT NEEDED HERE for RAMP
					   SetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, AMPLITUDE);      // moved from ReadAnalogInputs() to timer interrupt routine (here)  
					   clearArray();
					   GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE); 		// GENERATE RAMP
					   PlotMyArray ();
					   FREQUENCY_PREVI  = FREQUENCY;
					   //DUTY_CYCLE_PREVI = DUTY_CYCLE; 
					   AMPLITUDE_PREVI  = AMPLITUDE;
					 }
				  } // end of block when potentiometers are enabled
				sample_index_2=0;
			}
		}
		
	
		//PlotMyArray();  // plots in the upuuer graph, but SHOULD BE EXECUTED ONLY WHEN A CHANGE HAS OCCURED
		                  // MOVED TO THE ROUTINES SERVICING THE CONTROLS
         //things to add to the timer function
		/*
		1) continuous trace of square wave, resize the window based on sample rate.
		2) continue plotting square wave from where it ends, keep track of i.
		3) 
		*/
		}	
	}
	return (0);
}

//Button to Generate squarewave, with sample_rate, frequency, duty_cycle, base line, amplitude, no epoch
int CVICALLBACK Callback_Generate_Square (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			SetCtrlVal(MAIN_PANEL, PANEL_BINARYSWITCH_2, 1);
			    binary_switch_2=1;  // // indicates that we are working with square wave
				SetCtrlVal(MAIN_PANEL, PANEL_LED, 1);
				SetCtrlVal(MAIN_PANEL, PANEL_LED_2,0);
				PlotMyArray();
			   
			break;
	}
	return 0;
}

//Currently, the array is written to, but not redone when values are updated.
void GenerateSquareWave (double SR, double Freq, double Duty, double Base, double Amp){
	// preliminary calculations
    T = 1/Freq;  // period of the stimulation quare wave (in seconds)
	//EndSample =  RoundRealToNearestInteger (T*SR);   // floor() or ceil() may also be used
	//EndSample =  RoundRealToNearestInteger (T*SR);   
	EndSample =  floor(T*SR)-1;   
	MiddleSample =  RoundRealToNearestInteger(T*SR*Duty); 
	int index=0;
	int flag=0;
	int hold=0;
	double Ampli;
	

	if(MILKY_LCD==1){			// no signal modulation
	// Actual suare wave generation (populate square array 
	  for(int i= 0; i<MiddleSample ; i++){
		  Ampli =  Base+Amp ; 
		  if (Ampli>5.0) Ampli = 5.0;
		SQUARE_ARRAY[i]= Ampli;					   // fill first part with Base+Amp
	  }
	  for(int j= MiddleSample; j<(EndSample+1); j++){	   // fill second part with Base 
		SQUARE_ARRAY[j]= Base;
	  }
	}  // end of LCD generation
	
	else if(MILKY_LCD==0){	  // signal modulation ("chopping")
		for(int i=0; i<MiddleSample; i++){			      // fill first part with Base+Amp 
		  Ampli =  Base+Amp ; 
		  if (Ampli>5.0)  Ampli = 5.0;
		  SQUARE_ARRAY[i]= Ampli;
		}
		for(int j= MiddleSample; j<(EndSample+1); j++){     // fill second part with Base 
		    SQUARE_ARRAY[j]= Base;
	    }
		for(;index<MiddleSample;index++){
			if(index%MILKY_SPACING==0){		   // boundary reached, time to switch
				SQUARE_ARRAY[index]=Base-Amp;
				if(flag==0){							// high pulse
					for(int j=0; j<MILKY_SPACING; j++){
						Ampli =  Base-Amp; 
						if (Ampli<0.0) Ampli = 0.0;
						SQUARE_ARRAY[index+j]= Ampli;
						hold=j;
					}
					index+=hold;
					flag=1;
				}
				
				if(flag==1){							 // low pulse
					for(int j=0; j<MILKY_SPACING; j++){
						Ampli =  Base+Amp ; 
		                if (Ampli>5.0)  Ampli = 5.0;
						SQUARE_ARRAY[index+j+1]=Ampli;
						hold=j;
					}
					index+=hold;
					flag=0;
				}
			}
			}
	}
} // End of GenerateSquareWave


//Button to call VoidArray and plot the array.
int CVICALLBACK Callback_Plot_Array (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			//clearArray();   
			PlotMyArray ();
			break;
	}
	return 0;
}

//Plot "Square_Array" or "Ramp Array" onto graph 1
void PlotMyArray (void)
{
  DeleteGraphPlot(MAIN_PANEL, PANEL_GRAPH, -1, VAL_DELAYED_DRAW);
  if(binary_switch_2==1){
  	// PlotY(MAIN_PANEL, PANEL_GRAPH, SQUARE_ARRAY, TABLE_SIZE, VAL_DOUBLE, VAL_THIN_LINE, VAL_X, VAL_SOLID, TABLE_SIZE, VAL_GREEN); // plot the whole data array
	PlotY(MAIN_PANEL, PANEL_GRAPH, SQUARE_ARRAY, EndSample, VAL_DOUBLE, VAL_THIN_LINE, VAL_X, VAL_SOLID, 1, VAL_GREEN);	// plot only meaningful data
  }
  else if(binary_switch_2==0){
    // PlotY(MAIN_PANEL, PANEL_GRAPH, RAMP_ARRAY, TABLE_SIZE, VAL_DOUBLE, VAL_THIN_LINE, VAL_X, VAL_SOLID, TABLE_SIZE, VAL_GREEN);  // plot the whole data array
	PlotY(MAIN_PANEL, PANEL_GRAPH, RAMP_ARRAY, EndSample, VAL_DOUBLE, VAL_THIN_LINE, VAL_X, VAL_SOLID, 1, VAL_GREEN);      // plot only meaningful data 
  }
}


// !! This currently doesn't do anything, debugging numeric box used to actively plot the strip chart real time. 
int CVICALLBACK stripChartValue (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_STRIP_TEST, &STRIP_VAL); //epoch length not included
			break;
	}
	return 0;
}

																	
																				 
//Pause resume button interacts with FLAG variable; conditionally tied, toggles the LED
//to timer function. 
int CVICALLBACK TIMER_RUPT (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			if(STOP_FLAG==0){
				STOP_FLAG= 1;	
				//SetCtrlVal (MAIN_PANEL, PANEL_LED, 0);
			}
			else if (STOP_FLAG==1){
				STOP_FLAG=0;
				//SetCtrlVal (MAIN_PANEL, PANEL_LED, 1);
			}
			break;
	}
	return 0;
}



int CVICALLBACK timer_control (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	switch (event){
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLING_NUMERIC , &timer_numeric_value);
            snprintf(string_buffer, 5, "%f", timer_numeric_value);
		    SetCtrlVal(MAIN_PANEL, PANEL_STRING_SCALE,string_buffer);      // update time scale indicator
			SetCtrlAttribute(MAIN_PANEL, PANEL_TIMER, ATTR_INTERVAL, timer_numeric_value/1000);  // set timer interval
			SAMPLE_RATE =  RoundRealToNearestInteger (1/(timer_numeric_value/1000));
			MILKY_SPACING = RoundRealToNearestInteger (15.0/(timer_numeric_value));
			SetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, SAMPLE_RATE);
			break;
	}
	return 0;
}


int CVICALLBACK Callback_Generate_Ramp (int panel, int control, int event,
										void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			   GenerateRamp(SAMPLE_RATE, FREQUENCY, BASE_LINE, AMPLITUDE);
			   SetCtrlVal(MAIN_PANEL, PANEL_BINARYSWITCH_2, 0);
			   binary_switch_2=0;  // indicates that we are working with ramp
				SetCtrlVal(MAIN_PANEL, PANEL_LED, 0);
				SetCtrlVal(MAIN_PANEL, PANEL_LED_2,1);
				PlotMyArray();
			   
			break;
	}
	return 0;
}


//generates RAMP using only AMP and SR ; parameter Amp does not unclude the baseline
void GenerateRamp (double SR, double Freq, double Base, double Amp){

    RAMP_COUNTER=0;
	T = 1/Freq;  // period of the stimulation quare wave (in seconds)
	double currentAmp=(Amp/SR)*Freq;   // increment per sample
	//EndSample =  RoundRealToNearestInteger (T*SR);  
	EndSample =  floor(T*SR)-1;  
	double upTempo=0;  // the value to plot without the baseline
	
	
	// while(upTempo<Amp){
	for(i = 0; i<EndSample;i++){ 
		RAMP_ARRAY[i]= (Base+upTempo);		 // populate the array  considering the Base(line)
		//if(UPDATEAMP<Amp){
		//	UPDATEAMP+=currentAmp;
	//	}
		//PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, Base+Amp);
		upTempo+=currentAmp;		  // currentAmp is the increment per sample 
		 //printf(":%f", upTempo);
		RAMP_COUNTER++;
		//i++;
	}
	i=0;
	
	for(int i = RAMP_COUNTER; i< TABLE_SIZE;i++){  // erase the remainder of the signal array
		if(RAMP_ARRAY[i]!=Base){				// was 0
			//printf("%f ", RAMP_ARRAY[i]);
			RAMP_ARRAY[i]=Base;					// was 0
		//	printf("%d ", i);
		}
	}
	
} // End of GenerateRamp


//**************************************************************************************************************************************************************************
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//########################################################################################################################################################################## 
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
//**************************************************************************************************************************************************************************  


// ========================   HARDWARE I/O  using the NI DAQ USB-6009 ============================


/* ------------------------------   Analog Output   (DAC)  -------------------------------------- */             

  void AnalogOutput(int AnChannel, float64 data)
{

	int         error=0;
	// TaskHandle  taskHandleAO=0;		// global
	
    //char        chan[256];
	//char        chan[256] = {'Dev1/ao1'};
    // char        *chan  = "Dev1/ao1";  
	
	// char        *chan0  = "Dev1/ao0";	  // now made global 
	// char        *chan1  = "Dev1/ao1";	  // now made global 
	
	
	
	// double      min,max;		 // now global
	// float64     data;								 
	// char        errBuff[2048]={'\0'};	  // global
	
	
	
	// chan = 'Dev1/ao1';  // or Dev1/ao0, depending on  AnChannel
	
	//min =   0.0;   // -10.0;		  // globally initialized
	//max =   5.0;   //  10.0;
	
	
		/*********************************************/
		// DAQmx Configure Code
		/*********************************************/
		//SetWaitCursor(1);
		    // DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
		// DAQmxCreateTask("",&taskHandleAO);	//   moved to the initialization
		    //DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle,chan,"",min,max,DAQmx_Val_Volts,""));
		    //DAQmxCreateAOVoltageChan(taskHandle,chan,"",min,max,DAQmx_Val_Volts,"");
		/* if (AnChannel==0)  		  //   moved to the initialization           
		    DAQmxCreateAOVoltageChan(taskHandleAO,chan0,"",min,max,DAQmx_Val_Volts,"");   // output to AO chan 0   // should be separated?
		  else
			DAQmxCreateAOVoltageChan(taskHandleAO,chan1,"",min,max,DAQmx_Val_Volts,"");   // output to AO chan 1  
        */   


		  /*********************************************/
		  // DAQmx Start Code
		  /*********************************************/
		  //DAQmxErrChk (DAQmxStartTask(taskHandle));
		// DAQmxStartTask(taskHandleAO);  //   moved to the initialization           

		
		/*********************************************/
		// Actual DAQmx Write Code
		/*********************************************/
		//DAQmxErrChk (DAQmxWriteAnalogF64(taskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,&data,NULL,NULL));
		//  DAQmxWriteAnalogF64(taskHandleAO,1,1,10.0,DAQmx_Val_GroupByChannel,&data,NULL,NULL); //  Works!!!
		if (data>5.0)  data = 5.0;  
		//DAQmxWriteAnalogScalarF64 (taskHandleAO, 1, 10.0, data, 0); // Simpler; works too
		if (AnChannel==0) 
				   DAQmxWriteAnalogScalarF64 (taskHandleAO_0, 1, 10.0, data, 0);
		  else
			       DAQmxWriteAnalogScalarF64 (taskHandleAO_1, 1, 10.0, data, 0);

//   Attention: The channel is defined at the time of task creation!! Only one channel per task!
	

/*
Error:
	SetWaitCursor(0);													   
	if( DAQmxFailed(error) )
		DAQmxGetExtendedErrorInfo(errBuff,2048);
*/	
	//if( taskHandleAO!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		//DAQmxStopTask(taskHandleAO);	//   moved to the initialization           
		//DAQmxClearTask(taskHandleAO);   //   moved to the initialization           
	//}
	//if( DAQmxFailed(error) )
	//	MessagePopup("DAQmx Error",errBuff);
	
	
}  //  AnalogOutput  






void DAC_Test_NI_DAQmx (short An_Chan_Selected)  // test DAC output (from DEBUG panel to selected DAC channel)
 {
   		// float An_Voltage ;   // global, double 
   		
	 SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 1);    // turn LED ON              
		
	 //  Legacy NI-DAQ:
     //  Status = AO_Configure (1, DAC_Selected, 0, 0, 10.0, 0);   // Channel 0;  bipolar mode ; +/-10V (only); immediate update
	 // Status = AO_Configure (1, 0, 1, 0, 10.0, 0);   //             unipolar mode: illegal
     // if (Status != 0)
     //      printf("\nError in DAC (Analog output #0: code = %i\n",Status);
           //MessagePopup("Warning","Error in DAC (Analog output #0!");
       
	  // NI-DAQmx: (2015)
  //     DAQmxCreateTask(" ", &gTaskHandle);
  //     DAQmxCreateAOVoltageChan(gTaskHandle, chan," ",min,max,DAQmx_Val_Volts,NULL);
	  // DAQmxCfgSampClkTiming(gTaskHandle,"" ", rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, 1000);
	  
	  // Write 'data' to buffer:
	  // Writes multiple floating-point samples to a task that contains one or more analog output channels.
	  // Note: If you configured timing for your task, your write is considered a buffered write. 
	  // Buffered writes require a minimum buffer size of 2 samples. 
	  // If you do not configure the buffer size using DAQmxCfgOutputBuffer, NI-DAQmx automatically configures the buffer when 
	  //    you configure sample timing. If you attempt to write one sample for a buffered write without configuring the buffer,
	  //    you will receive an error. 
 //  DAQmxWriteAnalogF64(gTaskHandle,SampsPerCycle,0,10.0,DaQmx_Val_GroupByChannel,data,&written,NULL);
      
	  // Start generation (DAQmx):
 //  DAQmxStartTask(gTaskHandle);
	  
	  // Wait for 10 s until generation is complete
 //	  DAQmxWaitUntilTaskDone (gTaskHandle, 10.0);
	  
	  
	  // Stop and clear task
 //	  DAQmxStopTask(gTaskHandle);
 //	  DAQmxClearTask(gTaskHandle); 
	  
	  AnalogOutput(AnalogOutputChNmb,AnalogOutputValue);  // output -2.0 V  to AO 1  (second one)
	  
	  /*	   // Legacy NI-DAQ
      StopFlag=0; 
        for (time_out_counter=0; time_out_counter<1000; time_out_counter++)	 // no in-loop-delay
		  {
		  for (jj=-2047; jj<2047; jj++)
			 {   // both options work:
			     // slower option:
			        // An_Voltage = 10.0 / 2047 * jj;  // analog voltage (of type float)
				    // AO_VWrite (1, DAC_Selected, An_Voltage);   // expects voltage as double
				 // faster option:
			     //   AO_Write (1, DAC_Selected, jj); // Binary output:  -2047...+2047 (short) for +/-10V
			 }
			ProcessSystemEvents(); // takes about 25 ms to complete on the Lunchbox 2002!!
			                       // causes a plateau of that duration after the peak is reached!!
			                       // yet, enables interrupting the process by pressing the STOP control
			                       // can also be removed - then we will need to wait for time-out
			if (StopFlag) break; 
	      }
	   if (StopFlag) 
			  	{
			      //printf("\n\n Stop Button was pressed! \n\n"); // stop button was pressed
			      SoundAlarm(1);
			    }
		else  
			    {
			      //printf("\n\n Time Out occured! \n\n");	 // stop button was not pressed; time out
			      SoundAlarm(3);
			    } 
   */
	  
   
   SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 0);    // turn LED OFF              
   
   //AO_VWrite (1, DAC_Selected, 0.0);  // output a zero at the end    // Legacy DAQ
 }  // DAC_Test_NI_DAQmx



/* ------------------------------   Analog Input   (ADC)  -------------------------------------- */ 


void  ADC_Test_2(void)	     // Project-based (dynamic not NI-MAX-based
{							 // based on Acq-IntClk and AcqExtClk
 	int32       error=0;
	//TaskHandle  taskHandle=0;
	// char        chan[256];	   now global AIchan
	char 		clockSource[256];  // needed for externally paced ADC only
	float64     min,max,rate;
	// uInt32      sampsPerChan;		 // global
	int32       numRead;
	// uInt32      numChannels;			 // global
	float64     *data=NULL;
	//int         log;
	char        errBuff[2048]={'\0'};
	//uInt32      i, j, k, l, 
	uInt32     	m;
	int			PhysChan;
	//int 		InternalPace = 1;   // made global // 1 for internal pacing, 0 for external pacing, applied to PFI8 
	
	
	// InternalPace = 0;  // 1 for internal pacing, 0 for external pacing, applied to PFI8
	// if (InternalPace==0)
	//	Fmt(clockSource,"%s<Dev1/PFI8");  // use PFI8 as ADC pacing input can be done outside of this function
	
	 //sampsPerChan = 6000;    // should be global, or read from a global variable
	// sampsPerChan = numScans;  // numScansFFT + 1 scanning cycle
	
    // rate = 6000;
	// rate = SAMPLE_RATE;
	// AImin = -10.0;  AImax = 10.0;	 // now global
	//DeleteGraphPlot (DebugPanel,DEBUG_GRAPH_ONE_CHAN, -1, VAL_DELAYED_DRAW);
	DeleteGraphPlot (MAIN_PANEL,PANEL_GRAPH, -1, VAL_IMMEDIATE_DRAW);	  // NEEDS ATTENTION
	
	
	// PhysChan = Numb_AI_Chan;  // physical channel 0, or selected channel	  	  // moved to 'while' loop below
        //Fmt(chan,"%s<Dev1/port%i/line%i", port,line);
	    //Fmt(chan,"%s<Dev1/ai%i",PhysChan); 
	//Fmt(chan,"%s<Dev1/ai0,Dev1/ai1,Dev1/ai2,Dev1/ai3");	 // moved forward                      	    
	    //Fmt(chan,"%s<Dev1/ai0,Dev1/ai8");
	    //printf("  \n"); printf(chan);   printf("  \n");	
	
	/*********************************************/
		// DAQmx Configure Code
	/*********************************************/
		//SetWaitCursor(1);
		//DAQmxErrChk (DAQmxCreateTask("",&taskHandle));

	/*  TASK CREATION AND CONFIGURATION MOVED TO PROGRAM INITIALZATION   */	
		
	
	// allocate memory
	//if( (data=malloc(sampsPerChan*numChannels*sizeof(float64)))==NULL ) {
	//		MessagePopup("Error","Not enough memory");
	//		goto Error;
	//	}
	
	    m = sizeof(float64);
		// WaveformsLong != data;
		data = WaveformsLong;
		
		SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 1);  // turn on LED
		
        // Boris's LOOP: 
		StopFlag = 0;
		//j = 0;
	  while (!StopFlag)	 // works
		//  j++;
		//for (j=0; j<2; j++)
		{			  // start Boris's loop			
		PhysChan = Numb_AI_Chan;  
		ProcessSystemEvents();    		
		/*********************************************/
		// DAQmx Start Code
		/*********************************************/
		  //DAQmxErrChk (DAQmxStartTask(taskHandleADC2));		    // cannot be inside a loop!!
		//DAQmxStartTask(taskHandleADC2);	// MOVED TO INITIALIZATION																			  // NEEDS ATTENTION  
		  //SetCtrlAttribute(panel,PANEL_ACQUIRE,ATTR_DIMMED,1);
		  //ProcessDrawEvents();
		

		/*********************************************/
		// DAQmx Read Code
		/*********************************************/
		//DAQmxErrChk (DAQmxReadAnalogF64(taskHandleADC2,sampsPerChan,10.0,DAQmx_Val_GroupByChannel,data,sampsPerChan*numChannels,&numRead,NULL));
		// work with data pointer:
		//DAQmxReadAnalogF64(taskHandleADC2,sampsPerChan,10.0,DAQmx_Val_GroupByChannel,data,sampsPerChan*numChannels,&numRead,NULL);
		// work directly with WaveformsLong:
		//j = j;
	    DAQmxStartTask(taskHandleADC2); 
		DAQmxReadAnalogF64(taskHandleADC2,sampsPerChan,10.0,DAQmx_Val_GroupByChannel,WaveformsLong,sampsPerChan*numChannels,&numRead,NULL);	   	  // NEEDS ATTENTION  


		/* ReadAnalogF64 mode depending on pacing
		// internal or external pacing; data written to WaveformsLong
		if (InternalPace==0)   // external pacing
		  	DAQmxReadAnalogF64(taskHandleADC2,-1,10.0,DAQmx_Val_GroupByChannel,WaveformsLong,sampsPerChan*numChannels,&numRead,NULL);
		else   // internal pacing
		    DAQmxReadAnalogF64(taskHandleADC2,sampsPerChan,10.0,DAQmx_Val_GroupByChannel,WaveformsLong,sampsPerChan*numChannels,&numRead,NULL);	
		*/
		
		 DAQmxStopTask(taskHandleADC2); 		// Done upun QUITting
		
		//i = PhysChan;  // channel to plot
		//if( numRead>0 )
		//	for(i=0;i<numChannels;i++)  // k = m;
		//		PlotY(panel,PANEL_GRAPH,&(data[i*numRead]),numRead,VAL_DOUBLE,VAL_THIN_LINE,VAL_EMPTY_SQUARE,VAL_SOLID,1,plotColors[i%12]);
		//PlotY (DebugPanel, DEBUG_GRAPH_ONE_CHAN, &WaveformsLong[SelChan][StrtSmpl], 
        //   NmbDspSmpls, VAL_DOUBLE,VAL_THIN_LINE, 
        //   VAL_EMPTY_SQUARE, VAL_SOLID, 1, ColorArray[7]);// 5: Blue, 6:Magenta; 7:Red
		//PlotY (DebugPanel, DEBUG_GRAPH_ONE_CHAN, &(data[i*numRead]), 
        //   numRead, VAL_DOUBLE,VAL_THIN_LINE, 
        //   VAL_EMPTY_SQUARE, VAL_SOLID, 1, ColorArray[7]);// 5: Blue, 6:Magenta; 7:Red
		DeleteGraphPlot (MAIN_PANEL,PANEL_GRAPH, -1, VAL_IMMEDIATE_DRAW);  		          // NEEDS ATTENTION  
		//DeleteGraphPlot (MAIN_PANEL,PANEL_GRAPH, -1, VAL_DELAYED_DRAW);  		          // NEEDS ATTENTION
		PlotY (MAIN_PANEL, PANEL_GRAPH, &WaveformsLong[PhysChan][0], 
           numRead, VAL_DOUBLE,VAL_THIN_LINE, 
           VAL_EMPTY_SQUARE, VAL_SOLID, 1, ColorArray[7]);// 5: Blue, 6:Magenta; 7:Red
		
		// Update indicator
			AnalogInputValue = (WaveformsLong [Numb_AI_Chan][0] + WaveformsLong [Numb_AI_Chan][1] + WaveformsLong [Numb_AI_Chan][2]) / 3.0;
			SetCtrlVal(MAIN_PANEL, PANEL_NUMERIC_AI, AnalogInputValue); 
			     
		// FFT
		//FFT_analysis_one_channel (Numb_AI_Chan, numScansFFT);
		//PlotFFT() ;
		//k = k;
		
		//if (GenBuf[22] > AmpThreshold)  // simple test for 80 Hz (SR = 3750 Hz, 1024 points)
		//	   SetCtrlVal (DebugPanel, DEBUG_LED_CF, 1);  // turn on LED	
		//	else
		//	   SetCtrlVal (DebugPanel, DEBUG_LED_CF, 0);  // turn off LED	
			;		
	    } // Boris's test loop

		
Error:  	
  
	//k = 5;  // dummy
	//SetWaitCursor(0);
	//if( DAQmxFailed(error) )
	//	DAQmxGetExtendedErrorInfo(errBuff,2048);
	//if( taskHandle!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		// DAQmxStopTask(taskHandleADC2);
	    // DAQmxClearTask(taskHandleADC2);
		//k = 5;
		//SetCtrlAttribute(panel,PANEL_ACQUIRE,ATTR_DIMMED,0);
	//}
	
	
		
	SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 0);  // turn off LED      
	
	DAQmxClearTask(taskHandleADC2);  // // Done upun QUITting    
	
	// if(data) free(data);
	
	if( DAQmxFailed(error) )
		MessagePopup("DAQmx Error",errBuff);
	
   	

} // ADC_Test_2




int CVICALLBACK Callback_EnterAO (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				  // Numb_AO_Chan
				  GetCtrlVal(MAIN_PANEL, PANEL_NUMERIC_AO_CHAN, &Numb_AO_Chan);      
			break;
	}
	return 0;
}



int CVICALLBACK Callback_Enter_AO_Val (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				  GetCtrlVal(MAIN_PANEL, PANEL_NUMERIC_ENTER_AO_VAL, &AnalogOutputValue);
			break;
	}
	return 0;
}




int CVICALLBACK Callback_Update_AO (int panel, int control, int event,
									void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				 AnalogOutput(Numb_AO_Chan, AnalogOutputValue); 
			break;
	}
	return 0;
}



void GenerateRampToWaveformsLong (int ChNumbero)		// and Analog Output
{
  double TempAmp;
  
  SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 1);  // turn on LED      
  i = 0; StopFlag = 0;
  	  //while ((i<MaxScans) && (StopFlag== 0)) { 
	  while ((i<1000) && (StopFlag== 0)) {   	  
	  	  ProcessSystemEvents(); 
		  for (jj=0; jj<2047; jj++)
	    {   TempAmp = 5.0 / 2047.0 * jj;     //	jj;
			//WaveformsLong [ChNumbero][i] = TempAmp;  
		    AnalogOutput(ChNumbero, TempAmp);
	        //i++;
		    // if ((i==MaxScans) || (StopFlag))
			if (StopFlag)  
				break;
	    }
		i++;
	  } // end while loop
  AnalogOutput(ChNumbero, 0.0);		 // reset AO
  SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 0);  // turn on LED      
  SoundAlarm(2);
 } // GenerateRampToWaveformsLong 
											           


int CVICALLBACK Callback_STOP_Activate (int panel, int control, int event,
										void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
					//SetCtrlVal (MAIN_PANEL, PANEL_COMMANDBUTTON_STOP, 1); 
					StopFlag = 1;   // signal stop request
			break;
	}
	return 0;
}


// SetCtrlVal (MAIN_PANEL, PANEL_LED_DEBUG, 1);    // turn LED ON
// SetCtrlVal (MAIN_PANEL, PANEL_, 1);     


 void SoundAlarm(int numb_sounds)  // 2010
{ 
  for (i=0; i<numb_sounds; i++)
	  { Beep(); 
	    Delay(0.3); 
	  }
} // SoundAlarm();



int CVICALLBACK Callback_GenerateRamp (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
					GenerateRampToWaveformsLong (Numb_AO_Chan);
			break;
	}
	return 0;
}



int CVICALLBACK Callback_Select_AI_Channel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				 GetCtrlVal(MAIN_PANEL, PANEL_NUMERIC_AI_CHAN_SEL, &Numb_AI_Chan);
			break;
	}
	return 0;
}


int CVICALLBACK Callback_Measure_AI (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			ADC_Test_2();	
			AnalogInputValue = (WaveformsLong [Numb_AI_Chan][0] + WaveformsLong [Numb_AI_Chan][1] + WaveformsLong [Numb_AI_Chan][2]) / 3.0;
			SetCtrlVal(MAIN_PANEL, PANEL_NUMERIC_AI, AnalogInputValue); 
			break;
	}
	return 0;
}

//**************************************************************************************************************************************************************************
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//########################################################################################################################################################################## 
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
//**************************************************************************************************************************************************************************  

int CVICALLBACK binarySwitchControl2 (int panel, int control, int event,
									  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_BINARYSWITCH_2, &binary_switch_2);
			if(binary_switch_2==1){
				SetCtrlVal(MAIN_PANEL, PANEL_LED_2, 0);
				SetCtrlVal(MAIN_PANEL, PANEL_LED, 1);
				PlotMyArray();
			}
			else if(binary_switch_2==0){
				SetCtrlVal(MAIN_PANEL, PANEL_LED, 0);
				SetCtrlVal(MAIN_PANEL, PANEL_LED_2,1);
				PlotMyArray();
			}
			break;
	}
	return 0;
}

int CVICALLBACK milky_lcd_switch (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			if(MILKY_LCD==1){
				MILKY_LCD=0;
				//printf("MILKY\n");
			}
			else if(MILKY_LCD==0){
				MILKY_LCD=1;
				//printf("LCD\n");
			}

			break;
	}
	return 0;
}


//Left right eye switch
int CVICALLBACK Callback_SelectOutput (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_RINGSLIDE_SEL_OUTPUT, &EYE_SELECTOR);
			CloseOtherEye();
			//printf("%d \n", EYE_SELECTOR);													   
			break;
	}
	return 0;
}

//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*
//#*#*#*#*#*#*#*#*#*#*_____USELESS#*#*#*#*_____USELESS_____#*#*USELESS_____*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*
//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

int CVICALLBACK button_one (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				GetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON_2, EYE_STATE);
				if(EYE_STATE==0){
					SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON, 1);
				}
				SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON_2, 0);
				SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON_3, 0);
				EYE_SELECTOR=0;
				printf("%d\n", EYE_SELECTOR);

			break;
	}
	return 0;
}

int CVICALLBACK button_two (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON, 0);
			SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON_3, 0);
			EYE_SELECTOR=1;
			printf("%d\n", EYE_SELECTOR);
			
		break;
	}
	return 0;
}

int CVICALLBACK button_three (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON, 0);
				SetCtrlVal(MAIN_PANEL, PANEL_RADIOBUTTON_2, 0);
				EYE_SELECTOR=2;
				printf("%d\n", EYE_SELECTOR);

			break;
	}
	return 0;
}
//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*
//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*
//#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*


void ReadAnalogInputs()
{
 	int32       error=0;
	float64     rate, AverageAmplitude, AverageFrequency, AverageDuty;
	int32       numRead;
	float64     *data=NULL;
	char        errBuff[2048]={'\0'};
	uInt32     	m;
	int			PhysChan;

	
	PhysChan = Numb_AI_Chan;  // physical channel 0, or selected channel	  	  // NEEDS ATTENTION  
 
			 	
	 // ProcessSystemEvents();    		
	    DAQmxStartTask(taskHandleADC2); 
		DAQmxReadAnalogF64(taskHandleADC2,sampsPerChan,10.0,DAQmx_Val_GroupByChannel,WaveformsLong,sampsPerChan*numChannels,&numRead,NULL);	   	  // NEEDS ATTENTION  
	    DAQmxStopTask(taskHandleADC2); 		// Done upun QUITting
		
		
		// update variables double FREQUENCY, DUTY_CYCLE, AMPLITUDE
		AverageFrequency = WaveformsLong [0][0];
		if (AverageFrequency < 0 )
		  AverageFrequency = 0;
		FREQUENCY = MinFreq + MaxFreq * AverageFrequency / MaxInputAmplitudeInVolts ;
	    // SetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, FREQUENCY);  // to be moved to timer interrupt routine
		
		
		if (binary_switch_2==1)  // pot control of duty cycle only when working with square wave and not with a ramp   
		{
		  AverageDuty = WaveformsLong [1][0];
		  if (AverageDuty < 0)
		     AverageDuty = 0;
		  DUTY_CYCLE = MinDuty + MaxDuty * AverageDuty / MaxInputAmplitudeInVolts;
		  SetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, DUTY_CYCLE);  // display before dividing by 100	
		  DUTY_CYCLE = DUTY_CYCLE/100;	 // convert percentage to a floating point number
		  if(MILKY_LCD==1)   // LCD shutters are open at 0 V,  active low !!)
			   { DUTY_CYCLE = 1.00 - DUTY_CYCLE;   }
		 } // only when working with square wave and not with a ramp 
		 //l = l; // dummy
		 
		AverageAmplitude = WaveformsLong [2][0];
		if (AverageAmplitude < 0 )
		  AverageAmplitude = 0;
		//PreviousAmplitude =  AMPLITUDE; 
		TempAmplitude = MinAmplitude + MaxAmplitude * AverageAmplitude / MaxInputAmplitudeInVolts;
		RealAnalogOutputAmplitude = (TempAmplitude + BASE_LINE - 2.5) * 4;
		// AMPLITUDE = MinAmplitude + MaxAmplitude * AverageAmplitude / MaxInputAmplitudeInVolts;
		// if (AMPLITUDE>AOmax) AMPLITUDE = AOmax; // AOmax = 5 V (limit output amplitude to the range of the DAC)
		// SetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, AMPLITUDE);  // to be moved to timer interrupt routine
		if ((RealAnalogOutputAmplitude < 10.001) & (RealAnalogOutputAmplitude > -10.001))  // not using TempBaseline for protection yet !!
		{
		   SetCtrlVal (MAIN_PANEL, PANEL_NUMERIC_Output_Ampl, RealAnalogOutputAmplitude);
		   AMPLITUDE =  TempAmplitude;
		   SetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, AMPLITUDE); 
		}

Error:  	
	if( DAQmxFailed(error) )
		MessagePopup("DAQmx Error",errBuff);	
}  // ReadAnalogInputs



//  local variables used: AverageAmplitude, AverageFrequency, AverageDuty
/*	  Constants used above
#define MinFreq  0.2
#define MaxFreq  30  
#define MaxDuty  98	   // duty cycle in %
#define MinDuty  1
#define MinAmplitude  0.1
#define MaxAmplitude  10
#define MaxInputAmplitudeInVolts 10  // as read by the ADC
*/

int CVICALLBACK Callback_EnableDisablePotentiometers (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				  GetCtrlVal(MAIN_PANEL, PANEL_ENABLE_POTENTIOMETERS, &PotentiometersEnabled);
			break;
	}
	return 0;
}





int CVICALLBACK Callback_DebugProgram (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				l = l ;  // dummy statement, breakpoint for debugging purposes
			break;
	}
	return 0;
}


double AmplitudeOtherEye = 0;
unsigned char OpenOtherShutter = 1;  		 
unsigned char OpenBothShutters = 0;

	

void OpenOtherEye()
{   
	if(MILKY_LCD==1){ // if standard LCD, then send 0V to the shutter
		 // AmplitudeOtherEye = AMPLITUDE + BASE_LINE;
		 AmplitudeOtherEye = BASE_LINE;
			 if (AmplitudeOtherEye>5.0) AmplitudeOtherEye = 5.0;
		 switch (EYE_SELECTOR) {   // 0 = RE,  1 = LE;  2 = both eyes (channels
			case 0: AnalogOutput(1, AmplitudeOtherEye);  // if RE active, then send value to analog output 1 (LE)
					break;
		   	case 1: AnalogOutput(0, AmplitudeOtherEye);  // if LE active, then send value to analog output 0 (RE)
					break;
			//default:
			}	
			 
       } // standard LCD
     else  {  // "Milky" glass
		 OpenOtherShutter = 1;		// to be used elsewhere ?!  See GenerateSquareWaves
		 OpenBothShutters = 0;
       } // Milky	   
} // OpenOtherEye()


void CloseOtherEye()
{
	if(MILKY_LCD==1){ // if standard LCD, then send high voltage to the shutter
		 AmplitudeOtherEye = AMPLITUDE + BASE_LINE;
		 //AmplitudeOtherEye = BASE_LINE;
			 if (AmplitudeOtherEye>5.0) AmplitudeOtherEye = 5.0;
		 switch (EYE_SELECTOR) {   // 0 = RE,  1 = LE;  2 = both eyes (channels
			case 0: AnalogOutput(1, AmplitudeOtherEye);  // if RE active, then send value to analog output 1 (LE)
					break;
		   	case 1: AnalogOutput(0, AmplitudeOtherEye);  // if LE active, then send value to analog output 0 (RE)
					break;
			//default:
			}	
			 
       } // standard LCD
     else  {  // "Milky" glass, send 0 V to the shutter
		 //AmplitudeOtherEye = AMPLITUDE + BASE_LINE;
		 AmplitudeOtherEye = BASE_LINE;
			 if (AmplitudeOtherEye>5.0) AmplitudeOtherEye = 5.0;
		 switch (EYE_SELECTOR) {   // 0 = RE,  1 = LE;  2 = both eyes (channels
			case 0: AnalogOutput(1, AmplitudeOtherEye);  // if RE active, then send value to analog output 1 (LE)
					break;
		   	case 1: AnalogOutput(0, AmplitudeOtherEye);  // if LE active, then send value to analog output 0 (RE)
					break;
			//default:
			}		 
		 OpenOtherShutter = 0;		// to be used elsewhere ?!  See GenerateSquareWaves
		 OpenBothShutters = 0;
       } // Milky	
}  //  CloseOtherEye()



void OpenBothEyes()
{
	if(MILKY_LCD==1){ // if standard LCD, then send 0V to the shutter
		 //AmplitudeOtherEye = AMPLITUDE + BASE_LINE;
		 AmplitudeOtherEye = BASE_LINE;
			 if (AmplitudeOtherEye>5.0) AmplitudeOtherEye = 5.0;
		 	 AnalogOutput(0, AmplitudeOtherEye);  // if RE active, then send value to analog output 1 (LE)
			 AnalogOutput(1, AmplitudeOtherEye);  // if LE active, then send value to analog output 0 (RE)
       } // standard LCD
     else  {  // "Milky" glass
		 OpenOtherShutter = 1;		// to be used elsewhere ?!  See GenerateSquareWaves
		 OpenBothShutters = 1;
       } // Milky	
}  // OpenBothEyes()






int CVICALLBACK Callback_OpenOtherEye (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
					 OpenOtherEye();
			break;
	}
	return 0;
}


int CVICALLBACK Callback_CloseOtherEye (int panel, int control, int event,
										void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				CloseOtherEye();
			break;
	}
	return 0;
}



int CVICALLBACK Callback_OpenBothEyes (int panel, int control, int event,
									   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
				 OpenBothEyes();
			break;
	}
	return 0;
}
