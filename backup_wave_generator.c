#include <utility.h>
#include <ansi_c.h>
#include <userint.h>
#include "Wave_Generator.h"
#include "WaveUI.h"
#include <analysis.h>
#include <math.h>
#include <cvirte.h>

//Constants
#define TABLE_SIZE 2000
#define TWO_PI (3.14159 * 2)

static int MAIN_PANEL; //panel handle used to represent main window

//Square wave parameters
static double SAMPLE_RATE = 1000; //i.e 1 ms is SAMPLE_RATE= 1000;
static double FREQUENCY = 2; //Frequency of the waveform, per second HZ
static double EPOCH_LENGTH; //i.e 1 second, duration at peak amplitude.
static double DUTY_CYCLE = 0.5;
static double BASE_LINE = 0;
static double AMPLITUDE = 6;

static double SQUARE_ARRAY[TABLE_SIZE];
//static double PHASE= 0.0;
//static double *PTR_SQUARE_ARRAY=&SQUARE_ARRAY;
//static double *SQUARE_ARRAY_PHASE= &PHASE;

static double STRIP_VAL = 0;

int STOP_FLAG= 0;

double T; //Time= 1/Frequency for use in GenerateSquareWave
int MiddleSample, EndSample; //Middlesample and Endsample for use in GenerateSquareWave						    


// function declarations
void GenerateSquareWave (double SR, double Freq, double Duty, double Base, double Amp);  
void PlotMyArray (void) ;

int main(int argc, char *argv[]){
	
	if ((MAIN_PANEL= LoadPanel(0, "WaveUI.uir", PANEL)) <= 0 )
		return -1;
	
	//LoadPanel(int parentPanelHandle, char filename[], int panelResourceID);
	//Loads a panel into memory from UIR file, 0 makes panel parent, 
	
	DisplayPanel(MAIN_PANEL); //loadpanel loads into memory, display shows it
	RunUserInterface();
	//DiscardPanel(MAIN_PANEL);
}


void squareWaveStrip(double samp, double freq, double duty, double base, double amp){
	
	PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, STRIP_VAL);
}

int CVICALLBACK endProgram (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{													
	switch (event)
	{
		case EVENT_COMMIT:
			DiscardPanel (MAIN_PANEL); //removes from screen and memory
			CloseCVIRTE (); //releases memory allocated by CVI Runtime Engine
			if (QuitUserInterface(0)!= 0){// return on quit button press
				return -1;
			}
			break;
	}
	return 0;
}

//The sole purpose of processValues is to update, post factum, the variables
//that correspond to their respective numeric fields in the UIR.
int CVICALLBACK processValues (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE);
			GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE); 
			GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE); 
			
			printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			printf("FREQUENCY = %f\n", FREQUENCY);
			printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);
			printf("BASE_LINE = %f\n", BASE_LINE);
			printf("AMPLITUDE = %f\n", AMPLITUDE);
			
			break;
	}																  
	return 0;
}

//singularly updates the SAMPLE RATE real time.
int CVICALLBACK populateSampleRate (int panel, int control, int event,
									void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE); 
			//printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			break;
	}
	return 0;
}

//singularly updates the FREQUENCY
int CVICALLBACK populateFrequency (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			//printf("FREQUENCY = %f\n", FREQUENCY);     
			break;
	}
	return 0;
}

//singularly updates the EPOCH LENGTH. 
int CVICALLBACK populateEpochLength (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			//printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			break;
	}
	return 0;
}

//singularly updates the DUTY CYCLE
int CVICALLBACK populateDutyCycle (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			DUTY_CYCLE = DUTY_CYCLE/100.0;
			//printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);     
			break;
	}
	return 0;
}

//singularly updates the BASE LINE
int CVICALLBACK populateBaseLine (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE); 
			//printf("BASE_LINE = %f\n", BASE_LINE);
			break;
	}
	return 0;
}

//singularly updates the AMPLITUDE real time.
int CVICALLBACK populateAmplitude (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE);     
			//printf("AMPLITUDE = %f\n", AMPLITUDE);   
			break;
	}
	return 0;
}


int CVICALLBACK timerFunction (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2){
	if (event == EVENT_TIMER_TICK){
		if(STOP_FLAG == 0){
			GetCtrlVal(MAIN_PANEL,PANEL_STRIP_TEST, &STRIP_VAL);
		//  PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, STRIP_VAL);
		GenerateSquareWave(SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
		//debugging click/ printf("Click\n");
		PlotMyArray(); //things to add to the timer function
		/*
		1) continuous trace of square wave, resize the window based on sample rate.
		2) continue plotting square wave from where it ends, keep track of i.
		3) 
		*/
		}	
	}
	return (0);
}

//Currently, the array is written to, but not redone when values are updated.
void GenerateSquareWave (double SR, double Freq, double Duty, double Base, double Amp){
	// preliminary calculations
    T = 1/Freq;  // period of the stimulation quare wave (in seconds)
	EndSample =  RoundRealToNearestInteger (T*SR);   // floor() or cei() may also be used
	MiddleSample =  RoundRealToNearestInteger(T*SR*Duty);
	
	// Actual suare wave generation (populate square array 
	for(int i= 0; i<MiddleSample ; i++){
		SQUARE_ARRAY[i]= Base+Amp;
		PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, Base+Amp);
	}
	for(int j= MiddleSample; j<EndSample; j++){
		SQUARE_ARRAY[j]= Base;
			PlotStripChartPoint(MAIN_PANEL, PANEL_STRIPCHART, Base);
	}
} // End of GenerateSquareWave



//Button to call VoidArray and plot the array.
int CVICALLBACK Callback_Plot_Array (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			   PlotMyArray ();
			break;
	}
	return 0;
}

//Plot "Square_Array" onto graph 1
void PlotMyArray (void)
{
  DeleteGraphPlot(MAIN_PANEL, PANEL_GRAPH, -1, VAL_DELAYED_DRAW);	
  PlotY(MAIN_PANEL, PANEL_GRAPH, SQUARE_ARRAY, TABLE_SIZE, VAL_DOUBLE, VAL_THIN_LINE, VAL_X, VAL_SOLID, TABLE_SIZE, VAL_GREEN);	
}

//Button to Generate squarewave, with sample_rate, frequency, duty_cycle, base line, amplitude, no epoch
int CVICALLBACK Callback_Generate_Square (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			GenerateSquareWave (SAMPLE_RATE, FREQUENCY, DUTY_CYCLE, BASE_LINE, AMPLITUDE);
			//PlotMyArray();
			break;
	}
	return 0;
}


//Debugging numeric box used to actively plot the strip chart real time.
int CVICALLBACK stripChartValue (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_STRIP_TEST, &STRIP_VAL); //epoch length not included
			break;
	}
	return 0;
}


//Pause resume button interacts with FLAG variable; conditionally tied
//to timer function. 
int CVICALLBACK TIMER_RUPT (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2){
	switch (event){
		case EVENT_COMMIT:
			if(STOP_FLAG==0){STOP_FLAG= 1;}
			else if (STOP_FLAG==1){STOP_FLAG=0;}
			break;
	}
	return 0;
}
