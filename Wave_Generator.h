//==============================================================================
//
// Title:		Wave_Generator.h
// Purpose:		A short description of the interface.
//
// Created on:	7/1/2016 at 12:10:00 PM by rbsuser.
// Copyright:	. All Rights Reserved.
//
//==============================================================================

#ifndef __Wave_Generator_H__
#define __Wave_Generator_H__

#ifdef __cplusplus
    extern "C" {
#endif

//==============================================================================
// Include files

#include "cvidef.h"

//==============================================================================
// Constants

//==============================================================================
// Types

//==============================================================================
// External variables

//==============================================================================
// Global functions

int Declare_Your_Functions_Here (int x);

#ifdef __cplusplus
    }
#endif

#endif  /* ndef __Wave_Generator_H__ */
