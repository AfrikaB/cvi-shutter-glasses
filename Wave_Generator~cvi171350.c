#include <ansi_c.h>
#include <userint.h>
#include "Wave_Generator.h"
#include "WaveUI.h"
#include <analysis.h>
#include <math.h>
#include <cvirte.h>

//Constants
#define TABLE_SIZE 512
#define TWO_PI (3.14159 * 2)

static int MAIN_PANEL; //panel handle used to represent main window

//Square wave parameters
static double SAMPLE_RATE; //i.e 1 ms is SAMPLE_RATE= 1000;
static double FREQUENCY; //Frequency of the waveform, per second HZ
static double EPOCH_LENGTH; //i.e 1 second, duration at peak amplitude.
static double DUTY_CYCLE;
static double BASE_LINE;
static double AMPLITUDE;

static double SQUARE_ARRAY[TABLE_SIZE];
static double *PTR_SQUARE_ARRAY=&SQUARE_ARRAY;
static double PHASE= 0.0;
static double *SQUARE_ARRAY_PHASE= &PHASE;


int main(int argc, char *argv[]){
	
	if ((MAIN_PANEL= LoadPanel(0, "WaveUI.uir", PANEL)) <= 0 )
		return -1;
	
	//LoadPanel(int parentPanelHandle, char filename[], int panelResourceID);
	//Loads a panel into memory from UIR file, 0 makes panel parent, 
	
	DisplayPanel(MAIN_PANEL); //loadpanel loads into memory, display shows it
	RunUserInterface();
	//DiscardPanel(MAIN_PANEL);
}


int CVICALLBACK endProgram (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			DiscardPanel (MAIN_PANEL); //removes from screen and memory
			CloseCVIRTE (); //releases memory allocated by CVI Runtime Engine
			if (QuitUserInterface(0)!= 0){// return on quit button press
				return -1;
			}
			break;
	}
	return 0;
}

//The sole purpose of processValues is to update, post factum, the variables
//that correspond to their respective numeric fields in the UIR.
int CVICALLBACK processValues (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE);
			GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE); 
			GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE); 
			
			printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			printf("FREQUENCY = %f\n", FREQUENCY);
			printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);
			printf("BASE_LINE = %f\n", BASE_LINE);
			printf("AMPLITUDE = %f\n", AMPLITUDE);
			
			break;
	}
	return 0;
}

//singularly updates the SAMPLE RATE real time.
int CVICALLBACK populateSampleRate (int panel, int control, int event,
									void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_SAMPLE_RATE, &SAMPLE_RATE); 
			printf("SAMPLE_RATE = %f\n", SAMPLE_RATE);
			
			break;
	}
	return 0;
}

//singularly updates the FREQUENCY real time. 
int CVICALLBACK populateFrequency (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_FREQUENCY, &FREQUENCY);
			printf("FREQUENCY = %f\n", FREQUENCY);     

			break;
	}
	return 0;
}

//singularly updates the EPOCH LENGTH real time. 
int CVICALLBACK populateEpochLength (int panel, int control, int event,
									 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_EPOCH_LENGTH, &EPOCH_LENGTH);
			printf("EPOCH_LENGTH = %f\n", EPOCH_LENGTH);
			break;
	}
	return 0;
}

//singularly updates the DUTY CYCLE real time.
int CVICALLBACK populateDutyCycle (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_DUTY_CYCLE, &DUTY_CYCLE);
			printf("DUTY_CYCLE = %f\n", DUTY_CYCLE);     
			break;
	}
	return 0;
}

//singularly updates the BASE LINE real time.
int CVICALLBACK populateBaseLine (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_BASE_LINE, &BASE_LINE); 
			printf("BASE_LINE = %f\n", BASE_LINE);
			break;
	}
	return 0;
}

//singularly updates the AMPLITUDE real time.
int CVICALLBACK populateAmplitude (int panel, int control, int event,
								   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_VAL_CHANGED:
			GetCtrlVal(MAIN_PANEL, PANEL_AMPLITUDE, &AMPLITUDE);     
			printf("AMPLITUDE = %f\n", AMPLITUDE);   
			break;
	}
	return 0;
}

int CVICALLBACK plotWave (int panel, int control, int event,
						  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			SquareWave(TABLE_SIZE, AMPLITUDE, FREQUENCY, SQUARE_ARRAY_PHASE, DUTY_CYCLE, SQUARE_ARRAY);
			//PlotY(MAIN_PANEL, PANEL_GRAPH, PTR_SQUARE_ARRAY, TABLE_SIZE, VAL_FLOAT, VAL_THIN_LINE, VAL_X, VAL_SOLID, TABLE_SIZE, VAL_GREEN);
			break;
	}
	return 0;
}

int CVICALLBACK timerFunction (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	SquareWave(TABLE_SIZE, AMPLITUDE, FREQUENCY, SQUARE_ARRAY_PHASE, DUTY_CYCLE, SQUARE_ARRAY);
	
	if (event == EVENT_TIMER_TICK)
	{
		//SquareWave(TABLE_SIZE, AMPLITUDE, FREQUENCY, SQUARE_ARRAY_PHASE, DUTY_CYCLE, SQUARE_ARRAY); 
		printf("Click\n");
		DeleteGraphPlot(MAIN_PANEL, PANEL_GRAPH, -1, VAL_DELAYED_DRAW);	
		PlotWaveform(MAIN_PANEL, PANEL_GRAPH, SQUARE_ARRAY, TABLE_SIZE, VAL_DOUBLE, 1.0, 0.0 ,5.0, 1/FREQUENCY,VAL_THIN_LINE, VAL_EMPTY_SQUARE, VAL_SOLID, 1, VAL_DK_GREEN); 
		//PlotY(MAIN_PANEL, PANEL_GRAPH, PTR_SQUARE_ARRAY, TABLE_SIZE, VAL_FLOAT, VAL_THIN_LINE, VAL_X, VAL_SOLID, TABLE_SIZE, VAL_GREEN);
	}
	
	
	return (0);
}

//SQUARE WAVE UPDATE REWORK


int sgn(double d){
  if (d>=0) d=1;
   else d=-1;
  return d;
}

static void generate_square(const snd_pcm_channel_area_t *areas, 
                           snd_pcm_uframes_t offset,
                           int count, double *_phase)
{
    static double max_phase = 2. * M_PI;
    double phase = *_phase;
    double step = max_phase*freq/(double)rate;
    unsigned char *samples[channels];
    int steps[channels];
    unsigned int chn;
    int format_bits = snd_pcm_format_width(format);
    unsigned int maxval = (1 << (format_bits - 1)) - 1;
    int bps = format_bits / 8;  // bytes per sample 
    int phys_bps = snd_pcm_format_physical_width(format) / 8;
    int big_endian = snd_pcm_format_big_endian(format) == 1;
    int to_unsigned = snd_pcm_format_unsigned(format) == 1;
    int is_float = (format == SND_PCM_FORMAT_FLOAT_LE ||
                    format == SND_PCM_FORMAT_FLOAT_BE);
    double amplitude_scale = amplitude/8.56;


    // verify and prepare the contents of areas 
    for (chn = 0; chn < channels; chn++) {
            if ((areas[chn].first % 8) != 0) {
                    printf("areas[%i].first == %i, aborting...", chn , areas[chn].first);
                    exit(EXIT_FAILURE);
            }
            samples[chn] = (((unsigned char *)areas[chn].addr) + (areas[chn].first / 8));
           if ((areas[chn].step % 16) != 0) {
                   //debug printf("areas[%i].step == %i, aborting...  ", chn areas[chn].step);
                   exit(EXIT_FAILURE);
            }
            steps[chn] = areas[chn].step / 8;
            samples[chn] += offset * steps[chn];
    }
    // fill the channel areas 
    while (count-- > 0) {
            union {
                    float f;
                    int i;
            } fval;
            int res, i;
            if (is_float) {
		    int a = sgn(amplitude_scale * sin(phase) * maxval);
                    fval.f = (float) a;
                    res = fval.i;
            } else { 
		    int b = sgn(amplitude_scale * sin(phase) * maxval);
                    res = b;
	    }
                    

            if (to_unsigned)
                    res ^= 1U << (format_bits - 1);
            for (chn = 0; chn < channels; chn++) {
                    // Generate data in native endian format 
                    if (big_endian) {
                            for (i = 0; i < bps; i++)
                                    *(samples[chn] + phys_bps - 1 - i) = (res >> i * 8) & 0xff;
                    } else {
                            for (i = 0; i < bps; i++)
                                    *(samples[chn] + i) = (res >>  i * 8) & 0xff;
                    }
                    samples[chn] += steps[chn];
            }
            phase += step;
            if (phase >= max_phase)
                    phase -= max_phase;
    }
    *_phase = phase;

}
